import pytest
from users.models import CustomUser, UniversityUser
from utils.user.user_type_util import UserType

def test_is_valid_user_type_with_valid_user_type():
    user_type = CustomUser.super_user_type
    assert UserType.is_valid_user_type(user_type) == CustomUser.super_user_type

def test_is_valid_user_type_with_invalid_user_type():
    user_type = 'invalid_user_type'
    with pytest.raises(Exception) as exc_info:
        UserType.is_valid_user_type(user_type)
    assert 'User type ({user_type}) does not exist'.format(user_type=user_type) in str(exc_info.value)

def test_is_valid_user_type_with_valid_user_type_and_university_user_model():
    user_type = CustomUser.university_user_types[0]
    user_model = UniversityUser()
    assert UserType.is_valid_user_type(user_type, user_model) == user_type

def test_is_valid_user_type_with_invalid_user_type_and_university_user_model():
    user_type = 'invalid_user_type'
    user_model = UniversityUser()
    with pytest.raises(Exception) as exc_info:
        UserType.is_valid_user_type(user_type, user_model)
    assert 'Wrong User type ({user_type}) for this Model User ({user_model})'.format(user_type=user_type, user_model=user_model.__class__.__name__) in str(exc_info.value)

def test_is_valid_user_type_with_valid_user_type_and_custom_user_model():
    user_type = CustomUser.super_user_type
    user_model = CustomUser()
    assert UserType.is_valid_user_type(user_type, user_model) == user_type

def test_is_valid_user_type_with_invalid_user_type_and_custom_user_model():
    user_type = 'invalid_user_type'
    user_model = CustomUser()
    with pytest.raises(Exception) as exc_info:
        UserType.is_valid_user_type(user_type, user_model)
    assert 'Wrong User type ({user_type}) for this Model User ({user_model})'.format(user_type=user_type, user_model=user_model.__class__.__name__) in str(exc_info.value)
